############################################################
# Dockerfile to run OpenESB under JBoss Wildfly
# Based on Fedora Image
############################################################

FROM jboss/wildfly:latest
MAINTAINER David BRASSELY <brasseld@gmail.com>

# Set the OPENESB_VERSION env variable
ENV OPENESB_VERSION 1.0.0-SNAPSHOT
RUN cd $HOME && curl https://oss.sonatype.org/content/repositories/snapshots/net/open-esb/runtime/jboss7/openesb-as-embedded-modules/$OPENESB_VERSION/openesb-as-embedded-modules-1.0.0-20150424.091529-1.zip | bsdtar -xf - -C $HOME/wildfly/modules/

RUN cd $HOME/wildfly/standalone/deployments && curl -O https://oss.sonatype.org/content/repositories/snapshots/net/open-esb/admin/openesb-rest-api-war/1.0.2-SNAPSHOT/openesb-rest-api-war-1.0.2-20150424.032941-5.war

RUN cd $HOME/wildfly/standalone/deployments && curl -O https://oss.sonatype.org/content/repositories/snapshots/net/open-esb/admin/openesb-web-console/1.0.3-SNAPSHOT/openesb-web-console-1.0.3-20150424.035341-30.war

# Set the default command to run on boot
# This will boot WildFly with OpenESB modules in the standalone mode and bind to all interface
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]
